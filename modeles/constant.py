#!/usr/bin/python3
import time

#time
time_in_millis = lambda: int(round(time.time() * 1000))
fps = 60
time_interval = round(1000/30)
# window
window_width = 800
window_height = 600
canvas_width = 600
canvas_height = 600
# Road
ROAD_WIDTH = 20
LINE_WIDTH = 1
EXTERIOR_LINE_WIDTH = 2
LINE_DASH = (12, 4)

default_number_of_lane = 3
CURVE_WIDTH = 1.5 * ROAD_WIDTH

# Roads type
VERTICAL = 0
HORIZONTAL = 1
CURVE_NE = 2
CURVE_NW = 3
CURVE_SW = 4
CURVE_SE = 5

# Cars
car_radius = ROAD_WIDTH // 2 - LINE_WIDTH * 3
global_max_acceleration = 30
