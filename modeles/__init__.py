from pydantic import BaseModel


class SectionInformation(BaseModel):
    type: int
    direction: int = 0
    length: int = 1


class Coordinates(BaseModel):
    x: int
    y: int