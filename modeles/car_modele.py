#!/usr/bin/python3
import random
from modeles.constant import global_max_acceleration

randomint = lambda min, max: random.randint(min, max)


class Car:
    line = 0
    section = 0
    position = 0
    coordinates = None
    max_speed = 0
    speed = 0
    max_acceleration = 0
    acceleration = 0
    behavior = 0

    def __init__(self, max_speed, spawn_location):
        self.max_speed = max_speed
        self.speed = random.randint(60, self.max_speed)
        self.max_acceleration = randomint(5, global_max_acceleration)
        self.coordinates = spawn_location
        self.acceleration = 1
        self.line = 0
        self.section = 0
        self.id = -1

    def car_acceleration(self):
        if self.acceleration <= self.max_acceleration:
            self.acceleration += 1.05 * abs(self.acceleration) + 1
            if self.acceleration > self.max_acceleration:
                self.acceleration = self.max_acceleration

    def set_car_speed(self):
        self.speed -= self.acceleration
        if self.speed > self.max_speed:
            self.speed = self.max_speed

    def change_section(self):
        self.section += 1
        self.position = 0

    def set_id(self, car_id):
        self.id = car_id
