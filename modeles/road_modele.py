#!/usr/bin/python3
from modeles import Coordinates
from modeles.constant import (VERTICAL, HORIZONTAL,
                              CURVE_NE, CURVE_NW,
                              CURVE_SW, CURVE_SE
                              )


class Road:
    nb_of_sections = 0
    list_of_sections = []
    length_of_sections = []
    direction_of_sections = []
    nb_of_lines = 0
    first_section_start = [0, 0]
    sections_origins = []

    def __init__(self, road_size, nb_of_lines):
        self.nb_of_lines = nb_of_lines
        if road_size == "small":
            self.create_small_road()
        elif road_size == "medium":
            self.create_medium_road()
        else:
            self.create_large_road()

    def create_small_road(self):
        self.nb_of_sections = 5
        self.list_of_sections = [
            HORIZONTAL,
            CURVE_NE,
            VERTICAL,
            CURVE_SE,
            HORIZONTAL
        ]  # means, horizontal, curve to south, vertical, curve to east and horizontal
        self.length_of_sections = [400, 1, 300, 1, 0]
        self.direction_of_sections = [1, 0, 1, 1, 0]  # 1 : left to right, up to down and trigonometric direction else 0
        self.add_origin(Coordinates(x=0, y=50))

    def create_medium_road(self):
        self.nb_of_sections = 13
        self.list_of_sections = [
            HORIZONTAL,
            CURVE_SE,
            CURVE_NW,
            HORIZONTAL,
            CURVE_SE,
            VERTICAL,
            CURVE_NE,
            HORIZONTAL,
            CURVE_SW,
            CURVE_NW,
            HORIZONTAL,
            CURVE_NE,
            VERTICAL
        ]
        self.length_of_sections = [200, 1, 1, 20, 1, 80, 1, 170, 1, 1, 270, 1, 0]
        self.direction_of_sections = [1, 1, 0, 1, 1, 0, 1, 0, 0, 0, 1, 0,
                                      1]  # 1 : left to right, up to down and trigonometric direction else 0
        self.add_origin(Coordinates(x=0, y=500))

    def create_large_road(self):
        print("to implement")

    def get_section_type(self, i):
        return self.list_of_sections[i]

    def add_origin(self, coordinates: Coordinates):
        self.sections_origins.append(coordinates)

    def get_information_of_section(self, i):
        section = self.list_of_sections[i]
        origin = self.sections_origins[i]
        length = self.length_of_sections[i]
        direction = self.direction_of_sections[i]
        next_section = self.list_of_sections[i + 1]
        return section, origin, length, direction, next_section

    def get_information_of_section_curve(self, i):
        section = self.list_of_sections[i]
        origin = self.sections_origins[i - 1]
        length = self.length_of_sections[i]
        direction = self.direction_of_sections[i]
        previous_section = self.list_of_sections[i - 1]
        return section, origin, length, direction, previous_section
