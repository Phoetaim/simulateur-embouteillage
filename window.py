from tkinter import Canvas, Button, Frame, BOTH, Tk
from modeles.constant import ROAD_WIDTH, LINE_WIDTH, CURVE_WIDTH, canvas_height, canvas_width, car_radius, LINE_DASH, \
    EXTERIOR_LINE_WIDTH


def get_color(speed):
    color = 'violet'
    if speed > 150:
        color = 'red'
    elif speed > 130:
        color = 'orange'
    elif speed > 110:
        color = 'yellow'
    elif speed > 90:
        color = 'blue'
    elif speed > 70:
        color = 'indigo'
    return color


class Window(Frame):
    __slots__ = 'master', 'stay', 'UI',

    def __init__(self):
        master = Tk()
        master.geometry("800x600")
        Frame.__init__(self, master)
        self.master = master
        self.init_window()
        self.stay = True

    # Creation of init_window
    def init_window(self):
        self.UI = Canvas(self.master, bg="green", height=canvas_height, width=canvas_width)

        # changing the title of our master widget      
        self.master.title("GUI")

        # allowing the widget to take the full space of the root window
        self.pack(fill=BOTH, expand=1)

        # creating a button instance
        quit_button = Button(self, text="Quit", command=self.client_exit)

        # placing the button on my window
        quit_button.place(x=650, y=0)
        self.UI.place(x=0, y=0)

    def client_exit(self):
        self.stay = False

    def window_update(self):
        self.master.update()

    #########################
    ######### Roads #########
    #########################
    def draw_horizontal_road(self, origin, length, nb_of_lines):
        x = origin.x
        y = origin.y
        total_road_width = nb_of_lines * ROAD_WIDTH + LINE_WIDTH * nb_of_lines
        self.UI.create_rectangle(x, y, x + length, y + total_road_width, outline='gray', fill='gray')
        self.UI.create_line(x, y, x + length, y, width=EXTERIOR_LINE_WIDTH, fill='black')
        self.UI.create_line(x, y + total_road_width, x + length, y + total_road_width,
                            width=EXTERIOR_LINE_WIDTH, fill='black')
        for i in range(nb_of_lines - 1):
            self.UI.create_line(x, y + (LINE_WIDTH + ROAD_WIDTH) * (i + 1), x + length,
                                y + (LINE_WIDTH + ROAD_WIDTH) * (i + 1), width=LINE_WIDTH, fill='white', dash=LINE_DASH)

    def draw_vertical_road(self, origin, length, nb_of_lines):
        x = origin.x
        y = origin.y
        total_road_width = nb_of_lines * ROAD_WIDTH + LINE_WIDTH * nb_of_lines
        self.UI.create_rectangle(x, y, x + total_road_width, y + length, outline='gray', fill='gray')
        self.UI.create_line(x, y, x, y + length, width=EXTERIOR_LINE_WIDTH, fill='black')
        self.UI.create_line(x + total_road_width, y, x + total_road_width, y + length,
                            width=EXTERIOR_LINE_WIDTH, fill='black')
        for i in range(nb_of_lines - 1):
            self.UI.create_line(x + (LINE_WIDTH + ROAD_WIDTH) * (i + 1), y, x + (LINE_WIDTH + ROAD_WIDTH) * (i + 1),
                                y + length, width=LINE_WIDTH, fill='white', dash=LINE_DASH)

    def draw_curve(self, origin, angle, nb_of_lines):
        center_x = origin.x
        center_y = origin.y
        offset = nb_of_lines * ROAD_WIDTH + LINE_WIDTH * (nb_of_lines + 1) + CURVE_WIDTH
        self.UI.create_arc(center_x - offset, center_y - offset, center_x + offset, center_y + offset, start=angle,
                           extent=90, outline='gray', width=LINE_WIDTH, fill='gray')
        self.UI.create_arc(center_x - offset, center_y - offset, center_x + offset, center_y + offset, start=angle,
                           extent=90, outline='black', width=EXTERIOR_LINE_WIDTH, style='arc')
        offset -= (ROAD_WIDTH + LINE_WIDTH)
        for _ in range(nb_of_lines - 1):
            self.UI.create_arc(center_x - offset, center_y - offset, center_x + offset, center_y + offset, start=angle,
                               extent=90, outline='white', width=LINE_WIDTH, dash=LINE_DASH, style='arc')
            offset -= (ROAD_WIDTH + LINE_WIDTH)
        self.UI.create_arc(center_x - offset, center_y - offset, center_x + offset, center_y + offset, start=angle,
                           extent=90, outline='black', style='arc', width=EXTERIOR_LINE_WIDTH)
        offset -= LINE_WIDTH
        self.UI.create_arc(center_x - offset, center_y - offset, center_x + offset, center_y + offset, start=angle,
                           extent=90, outline='green', width=LINE_WIDTH, fill='green')

    ########################
    ######### Cars #########
    ########################

    def draw_car(self, coordinates, speed):
        color = get_color(speed)
        return self.UI.create_arc(coordinates.x - car_radius, coordinates.y - car_radius,
                                  coordinates.x + car_radius, coordinates.y + car_radius,
                                  start=0, extent=359, outline=color, fill=color)

    def update_car(self, car_id, coordinates, speed):
        color = get_color(speed)
        self.UI.itemconfig(car_id, fill=color, outline=color)
        self.UI.coords(car_id,
                       coordinates.x - car_radius, coordinates.y - car_radius,
                       coordinates.x + car_radius, coordinates.y + car_radius)
