from modeles import SectionInformation, Coordinates
from modeles.road_modele import Road
from modeles.constant import *


class RoadController:
    nb_of_lane = default_number_of_lane

    def __init__(self, road_size, nb_of_lines, window):
        self.road = Road(road_size, nb_of_lines)
        self.window = window
        self.nb_of_lines = nb_of_lines
        self.determine_origins_of_sections()
        self.end_road()

    def determine_origins_of_sections(self):
        for i in range(self.road.nb_of_sections - 1):
            section_type, origin, length, direction, next_section = self.road.get_information_of_section(i)
            if section_type == VERTICAL:
                self.determine_origin_for_vertical(origin, length, direction, next_section)
            elif section_type == HORIZONTAL:
                self.determine_origin_for_horizontal(origin, length, direction, next_section)
            else:
                section_type, origin, length, direction, next_section = self.road.get_information_of_section_curve(i + 1)
                if section_type == VERTICAL:
                    self.determine_origin_curve_next_vertical(origin, length,
                                                              direction, next_section)
                elif section_type == HORIZONTAL:
                    self.determine_origin_curve_next_horizontal(origin, length,
                                                                direction, next_section)
                else:
                    self.determine_origin_curve_next_curve(self.road.sections_origins[i],
                                                           self.road.direction_of_sections[i],
                                                           self.road.direction_of_sections[i + 1],
                                                           self.road.list_of_sections[i])

    def determine_origin_for_horizontal(self, origin, length, direction, next_section):
        new_origin = Coordinates(x=origin.x + length * direction, y=origin.y)
        if next_section == CURVE_NE or next_section == CURVE_NW:
            origin_south = self.road.nb_of_lines * ROAD_WIDTH + LINE_WIDTH * (self.road.nb_of_lines + 1) + CURVE_WIDTH
            new_origin.y = origin.y + origin_south
        elif next_section == CURVE_SW or next_section == CURVE_SE:
            origin_north = CURVE_WIDTH + LINE_WIDTH
            new_origin.y = origin.y - origin_north
        self.road.add_origin(new_origin)

    def determine_origin_for_vertical(self, origin, length, direction, next_section):
        new_origin = Coordinates(x=origin.x, y=origin.y + length * direction)
        if next_section == CURVE_NW or next_section == CURVE_SW:
            origin_east = self.road.nb_of_lines * ROAD_WIDTH + LINE_WIDTH * (self.road.nb_of_lines + 3) + CURVE_WIDTH
            new_origin.x = origin.x + origin_east
        elif next_section == CURVE_NE or next_section == CURVE_SE:
            origin_west = CURVE_WIDTH + LINE_WIDTH
            new_origin.x = origin.x - origin_west
        self.road.add_origin(new_origin)

    def determine_origin_curve_next_horizontal(self, origin, length, direction, previous_section):
        new_origin = Coordinates(x=origin.x + length * (direction - 1), y=origin.y)
        if previous_section == CURVE_NE or previous_section == CURVE_NW:
            origin_south = self.road.nb_of_lines * ROAD_WIDTH + LINE_WIDTH * (self.road.nb_of_lines + 1) + CURVE_WIDTH
            new_origin.y = origin.y - origin_south
        elif previous_section == CURVE_SW or previous_section == CURVE_SE:
            origin_north = CURVE_WIDTH + LINE_WIDTH
            new_origin.y = origin.y + origin_north
        self.road.add_origin(new_origin)

    def determine_origin_curve_next_vertical(self, origin, length, direction, previous_section):
        new_origin = Coordinates(x=origin.x, y=origin.y + length * (direction - 1))
        if previous_section == CURVE_NW or previous_section == CURVE_SW:
            origin_east = self.road.nb_of_lines * ROAD_WIDTH + LINE_WIDTH * (self.road.nb_of_lines + 1) + CURVE_WIDTH
            new_origin.x = origin.x + origin_east
        elif previous_section == CURVE_NE or previous_section == CURVE_SE:
            origin_west = CURVE_WIDTH + LINE_WIDTH
            new_origin.x = origin.x + origin_west
        self.road.add_origin(new_origin)

    def determine_origin_curve_next_curve(self, origin, direction1, direction2, section):
        if direction1 == direction2:
            self.road.add_origin(origin)
            return
        new_origin = Coordinates(x=origin.x, y=origin.y)
        offset = 2 * CURVE_WIDTH + self.nb_of_lines * ROAD_WIDTH + (self.nb_of_lines + 2) * LINE_WIDTH
        if (section == CURVE_NE and direction1 == 0) or (section == CURVE_SE and direction1 == 1):
            new_origin.x += offset
        elif (section == CURVE_SW and direction1 == 0) or (section == CURVE_NW and direction1 == 1):
            new_origin.x -= offset
        elif (section == CURVE_SW and direction1 == 0) or (section == CURVE_SE and direction1 == 0):
            new_origin.y += offset
        elif (section == CURVE_NE and direction1 == 1) or (section == CURVE_NW and direction1 == 0):
            new_origin.y -= offset
        self.road.add_origin(new_origin)

    def end_road(self):
        if (self.road.list_of_sections[-1] == VERTICAL) and (self.road.direction_of_sections[-1] == 1):
            self.road.length_of_sections[-1] = canvas_height - self.road.sections_origins[-1].y
        elif (self.road.list_of_sections[-1] == VERTICAL) and (self.road.direction_of_sections[-1] == 0):
            self.road.sections_origins[-1].y = 0
            self.road.length_of_sections[-1] = self.road.sections_origins[-2].y
        elif (self.road.list_of_sections[-1] == HORIZONTAL) and (self.road.direction_of_sections[-1] == 1):
            self.road.length_of_sections[-1] = canvas_width - self.road.sections_origins[-1].x
        elif (self.road.list_of_sections[-1] == HORIZONTAL) and (self.road.direction_of_sections[-1] == 0):
            self.road.sections_origins[-1].x = 0
            self.road.length_of_sections[-1] = self.road.sections_origins[-2].x

    def draw_road(self):
        for i in range(self.road.nb_of_sections):
            origin = self.road.sections_origins[i]
            length = self.road.length_of_sections[i]
            section_type = self.road.get_section_type(i)
            if section_type == VERTICAL:
                self.window.draw_vertical_road(origin, length, self.nb_of_lines)
            elif section_type == HORIZONTAL:
                self.window.draw_horizontal_road(origin, length, self.nb_of_lines)
            else:
                angle = 90 * (section_type - 2)
                self.window.draw_curve(origin, angle, self.nb_of_lines)

    def get_road_information(self, section_index):
        return SectionInformation(type=self.road.list_of_sections[section_index],
                                  direction=self.road.direction_of_sections[section_index],
                                  length=self.road.length_of_sections[section_index])
