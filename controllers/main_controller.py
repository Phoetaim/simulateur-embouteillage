#!/usr/bin/python3
from controllers.car_controller import CarController
from controllers.road_controller import RoadController
from modeles import Coordinates
from modeles.constant import time_in_millis, time_interval, LINE_WIDTH, ROAD_WIDTH, VERTICAL, HORIZONTAL, \
    default_number_of_lane
from window import Window


class MainController:

    def __init__(self):
        self.app = Window()
        self.max_speed = 200
        self.traffic = 1
        self.last_spawn = 0
        self.car_list = []
        self.road_controller = RoadController('medium', default_number_of_lane, self.app)
        self.road_controller.draw_road()
        self.timer = time_in_millis()
        self.spawn_location = Coordinates(x=0, y=0)
        self.determine_car_spawn_location()
        self.car_spawn()
        self.main_loop()

    def main_loop(self):
        while self.app.stay:
            if time_in_millis() - self.timer > time_interval:
                self.list_car_movement()
                self.app.window_update()
                self.timer = time_in_millis()
                if time_in_millis() - self.last_spawn > 15000 / (self.traffic + 1):
                    self.car_spawn()

    def list_car_movement(self):
        for car_controller in self.car_list:
            car = car_controller.car
            section_information = self.road_controller.get_road_information(
                car.section)
            car_controller.car_movement(section_information)
            self.app.update_car(car.id, car.coordinates, car.speed)

    def determine_car_spawn_location(self):
        x = 0
        y = 0
        section = self.road_controller.get_road_information(0)
        big_offset = LINE_WIDTH * self.road_controller.nb_of_lines + round(
            ROAD_WIDTH * (self.road_controller.nb_of_lines - 0.5))
        if section.direction == 1 and section.type == VERTICAL:
            x = self.road_controller.road.sections_origins[0].x + LINE_WIDTH + round(ROAD_WIDTH / 2)
        elif section.direction == 0 and section.type == VERTICAL:
            x = self.road_controller.road.sections_origins[0].x + big_offset
        elif section.direction == 1 and section.type == HORIZONTAL:
            y = self.road_controller.road.sections_origins[0].y + big_offset
        elif section.direction == 0 and section.type == HORIZONTAL:
            y = self.road_controller.road.sections_origins[0].y + LINE_WIDTH + round(ROAD_WIDTH / 2)
        self.spawn_location.x = x
        self.spawn_location.y = y

    def car_spawn(self):
        new_car = CarController(self.max_speed, self.spawn_location)
        car_id = self.app.draw_car(self.spawn_location, new_car.car.speed)
        new_car.car.set_id(car_id)
        self.car_list.append(new_car)
        self.last_spawn = time_in_millis()

    def user_exit(self):
        exit()
