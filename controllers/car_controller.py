from math import cos, pi, sin

from modeles import SectionInformation
from modeles.car_modele import Car
from modeles.constant import VERTICAL, HORIZONTAL


class CarController:
    __slots__ = 'car'

    def __init__(self, max_speed, spawn_location):
        self.car = Car(max_speed, spawn_location)

    def car_movement(self, section: SectionInformation):
        if section.type == VERTICAL:
            self.car_vertical_movement(section.direction)
        elif section.type == HORIZONTAL:
            self.car_horizontal_movement(section.direction)
        else:
            self.car_curve_movement(section.type, section.direction)
        if self.car.position >= section.length:
            self.car.change_section()

    def car_vertical_movement(self, direction):
        self.car.coordinates.y += round(self.car.speed * (direction * 2 - 1) / 30)
        self.car.position = self.car.coordinates.y

    def car_horizontal_movement(self, direction):
        self.car.coordinates.x += self.car.speed * (direction * 2 - 1) / 20
        self.car.position = self.car.coordinates.x

    def car_curve_movement(self, section_type, direction):
        self.car.position += 1/self.car.speed
        self.car.coordinates.x += cos(-pi/4 + self.car.position)
        self.car.coordinates.y += sin(-pi/4 + self.car.position)
